package models


import java.nio.channels.UnsupportedAddressTypeException
import java.sql.SQLException
import java.util.regex.{Matcher, Pattern}

import anorm.{SQL, ~}
import anorm.SqlParser.str
import play.api.libs.json.Json
import play.api.libs.Codecs.sha1

import scala.concurrent.Future

case class Record(
                   sha1: String,
                   url: String
                 )


object Record extends ((
    String,
    String
  ) => Record) {

  private val httpRegex = "(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]\\.[^\\s]{2,})"
  private val wwwRegex = "www[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}"
  private val regularRegex = "[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]\\.[^\\s]{2,}"

  implicit val jsonFormat = Json.format[Record]

  val records =
    str("sha1") ~
      str("url") map {
      case     sha1~url =>
        Record(sha1,url)
    }

  def getByHashCode(sha1:String) = Future {
    DB.withConnection { implicit connection =>
      SQL(
        """
          SELECT
             sha1,
             url
          FROM urlRecords
          WHERE sha1 = {sha1};
        """
      ).on(
        'sha1 -> sha1
      ).as(records.singleOpt)
    }
  }

  def getByUrl(url:String) = Future {
    DB.withConnection { implicit connection =>
      SQL(
        """
          SELECT
             sha1,
             url
          FROM urlRecords
          WHERE url = {url};
        """
      ).on(
        'url -> url
      ).as(records.singleOpt)
    }
  }

  def getAllRecords() = Future {
    DB.withConnection { implicit connection =>
      SQL(
        """
          SELECT
            sha1,
            url
          FROM urlRecords
        """
      ).as(records *)
    }
  }

  def generateSha1(inputUrl: String): String =  {
    val msdDigest = java.security.MessageDigest.getInstance("SHA-1")
    msdDigest.update(inputUrl.getBytes("UTF-8"), 0, inputUrl.length())
    sha1(msdDigest.digest(inputUrl.getBytes())).substring(0,6)
  }

  def adjustUrl(url: String): String = {
    val httpMatcher = Pattern.compile(httpRegex).matcher(url)
    val wwwMatcher = Pattern.compile(wwwRegex).matcher(url)
    val regularMatcher = Pattern.compile(regularRegex).matcher(url)
    if (httpMatcher.matches) url
    else if (wwwMatcher.matches) "http://" + url
    else if (regularMatcher.matches && url.indexOf("www.") != 0) "http://www." + url
    else throw new UnsupportedAddressTypeException
  }

  def countAll() = Future {
    DB.withConnection { implicit connection =>
      val result = SQL(
        """
          SELECT COUNT(1) count
          FROM urlRecords r;
        """
      ).apply()

      try {
        Some(result.head[Long]("count"))
      } catch {
        case e:Exception => None
      }
    }
  }

  def create(url:String) = Future {
    val sha1 = generateSha1(adjustUrl(url))
    try {
      DB.withConnection { implicit connection =>
        SQL(
          """
          INSERT INTO urlRecords (
            sha1,
            url
          ) VALUES (
            {sha1},
            {url}
          );
        """
        ).on(
          'sha1 -> sha1,
          'url -> url
        ).executeInsert()
        sha1
      }
    } catch{
      case sqe: Exception => sqe
    }
  }


  def deleteAndCreate() = Future{
    DB.withConnection { implicit connection =>
      SQL(
        """
          DELETE
          FROM urlRecords
        """).executeUpdate();
    }
  }

}




