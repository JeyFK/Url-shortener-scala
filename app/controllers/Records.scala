package controllers

import java.sql.SQLException

import models.Record
import play.api.Logger
import play.api.data._
import play.api.data.Forms._
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.Future

object Records extends Controller {

  val CREATION_ERROR = "Url already exist in DB, you can find it in the list"
  val BAD_REQUEST_REASON = "Could not bind POST data to form."


  def getRecordByHash(sha1:String) = Action.async {
    Record.getByHashCode(sha1) map {
      case Some(record: Record) => Redirect(record.url)
      case _ => NoContent
    }
  }

  def getAllRecords = Action.async {
    Record.getAllRecords map {
      case records:List[Record] => Ok(Json.toJson(records))
      case _ => NoContent
    }
  }

  def post = Action.async {
    implicit request =>

      val form = Form(
        "url" -> text
      )
      form.bindFromRequest match {
        case form:Form[String] if !form.hasErrors => {
          val url = form.get
          Record.create(url) map {
            case sha1:String => Created(Json.obj("sha1" -> sha1));
            case None |  _ => Accepted(Json.obj("error" -> CREATION_ERROR))
          }
        }
        case _ => Future { BadRequest(Json.obj("reason" -> BAD_REQUEST_REASON)) }
      }
  }

}
