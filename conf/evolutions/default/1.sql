# Users schema
# --- !Ups

CREATE TABLE urlRecords(
  sha1      VARCHAR(255) NOT NULL PRIMARY KEY ,
  url       VARCHAR(255) NOT NULL
);

# --- !Downs

DROP TABLE IF exists urlRecords