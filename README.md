# URL shortener

App for shortening urls, similar to www.goo.gl

## Getting Started
Application is developed using Scala, SQLite, and Playframework.

Simple app, just provide a valid url and you'll receive shortened version of it.
Click "Show urls" button to display all records stored in DB.


### Running up
'sbt run' for running
Application will ran on localhost:9000


## Running the tests

'sbt test' for running unit tests. all data purged from test.db on the start of test run.

## Authors

* **Vladyslav Danin** - *Initial work* - [JeyFK](https://gitlab.com/JeyFK)

