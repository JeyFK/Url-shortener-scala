$('input#create').on('mousedown',function() {
  var url = $('input#generate-url').val();
  if (!url) return;
  var options = {
    url: '/record',
    method: 'POST',
    data: {
      "url" : url
    },
    success: function(data,status,xhr) {
    if (data.error) {
      showExc(data.error);
      console.log('unable to create record');
      return
    }
    console.log('created successfully');
    $(".success").show()
    },
    error: function(xhr,status,ex) {
      console.log('created unsuccessfully');
      console.log('status: ' + status);
      console.log('ex: ' + ex);
    }
  };
  $.ajax(options);

});

function showExc(exc) {
  if (!exc) {
    $('#error').hide();
    return;
  }
  $('#error').show();
  $('#error').html(exc);
  return;
};


$('#show').on('mousedown',function() {

  setTimeout(function() {

    var options = {
      url: '/records',
      method: 'GET',
      success: function(data,status,xhr) {
        console.log('searched successfully');

        var n = data.length;

        $('#no-record-selected').hide();
        $('.search-results').show();

        for (var i = 0; i < n; i++) {
          $('#record-selected').append("<h2 class='(\"" + data[i].sha1 + "\");'>" +
           data[i].url + "</h2> <a href=/hash/" + data[i].sha1 + ">" + data[i].sha1 + "</a>");
        }
      },
      error: function(xhr,status,ex) {
        console.log('searched unsuccessfully');
        console.log('status: ' + status);
        console.log('ex: ' + ex);
      }
    };

    $.ajax(options);
  },100);
});