package test

import play.api.test._
import scala.concurrent.{ExecutionContext}

import org.specs2.execute.{Result, AsResult}

abstract class App(app:FakeApplication = App.app) extends WithApplication(app) {
  override def around[T: AsResult](t: => T): Result = super.around {
    wipeData()
    val result = t
    result
  }

  def wipeData() {
   val wiped = sync { models.Record.deleteAndCreate()}
  }
}

object App {

  def app = FakeApplication(additionalConfiguration =
    Map(
      "db.default.driver" -> "org.sqlite.JDBC",
      "db.default.url" -> "jdbc:sqlite:db/test.db?mode=memory&cache=shared&pooling=true",
      "evolutionplugin" -> "enabled",
      "applyEvolutions.default" -> "true",
      "applyDownEvolutions.default" -> "true"
    )
  )

}
