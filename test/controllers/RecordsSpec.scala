package controllers

import models.Record
import test._
import play.api.Logger
import play.api.mvc._
import play.api.test._
import play.api.test.Helpers._
import play.api.libs.json._
import play.api.libs.json.Json._

import scala.concurrent.{Await, ExecutionContext}

class RecordsSpec extends Specification {

  def create(urls:String *):List[String] = {

    val header = FakeRequest(POST, "/record")

    urls.foldLeft(List[String]()) {
      (list,url) =>
      val body = Json.obj(
        "url" -> url
      )

      val response = route(header,body).get

      status(response) mustEqual 201
      val json = Json.parse(contentAsString(response))
      json \ "sha1" match {
        case JsString(sha1) => list :+ sha1.toString
        case JsBoolean(false) => {
          failure("Could not shorter Url")
          Nil
        }
        case _ => {
          failure("Unknown failure")
          Nil
        }
      }
    }
  }

  "GET /hash/:sha1" should {

    "return record if sha1 exists for it" in new App {

      val sha1 = create("www.google.com","www.apple.com","mid.org","goog.le").head

      val request = FakeRequest(GET, "/hash/" + sha1)

      val response = route(request).get

      status(response) mustEqual 303
    }

    "return no content because there are no record by selected sha1" in new App {

      val request = FakeRequest(GET, "/hash/21312412412")

      val response = route(request).get

      status(response) mustEqual 204
    }
  }

  "return error if url exists" in new App {
    var url = "www.whitehouse.com"
    val body = Json.obj(
      "url" -> url
    )
    val header = FakeRequest(POST, "/record")

    val response = route(header,body).get

    status(response) mustEqual 201

    val headerDupl = FakeRequest(POST, "/record")

    val responseDupl = route(headerDupl,body).get

    status(responseDupl) mustEqual 202
    val json = Json.parse(contentAsString(responseDupl))
    json \ "error" match {
      case JsString(error) => error.toString mustEqual Records.CREATION_ERROR

      case _ => {
        failure("Unknown failure")
        Nil
      }
    }
  }

  "All records are returned" in new App {
    val params = List("www.google.com", "www.apple.com", "mid.org", "goog.le")
    create("www.google.com", "www.apple.com", "mid.org", "goog.le")
    var list = List[String]()
    val request = FakeRequest(GET, "/records")

    val response = route(request).get

    status(response) mustEqual 200

    val json = Json.parse(contentAsString(response))
    json match {
      case JsArray(array) => array.length mustEqual params.size
      case _ => {
        failure("Unknown failure")
        Nil
      }
    }
  }


  "POST /record" should {

    "shorter new url" in new App {

      create("www.google.com")
    }
  }

}