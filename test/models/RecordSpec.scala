package models

import java.nio.channels.UnsupportedAddressTypeException

import test._
import play.api.mvc._
import play.api.test._
import play.api.test.Helpers._
import play.api.libs.json._
import play.api.libs.json.Json._

import scala.concurrent.{Await, ExecutionContext}

object RecordSpec extends Specification {

  def create(urls: String*): List[String] = {

    urls.foldLeft(List[String]()) {
      (list, url) =>

        val sha1: String = sync {
          Record.create(url) map {
            case sha1: String => sha1
            case _ => failure("Error, url was not shortened")
          }
        }

        list :+ sha1
    }

  }


  "Record.getByHashCode" should {

    "return record, if hashCode exists" in new App {
      val url = "www.apple.com/iphone-7"
      val sha1 = create(url).head

      val result = Record.getByHashCode(sha1) map {
        case Some(record: Record) => {
          record.sha1 mustEqual sha1
          record.url mustEqual url
          success
        }
        case _ => failure("This sha1 should exist")
      }

      sync {
        result
      }
    }


    "return nothing if no such hashCode found" in new App {

      val result = Record.getByHashCode("no record exist") map {
        case Some(record: Record) => failure("Record with such hashcode does not exist")
        case _ => success
      }

      sync {
        result
      }
    }
  }

  "Record.getRecordByUrl" should {

    "return record, if url exists" in new App {
      val url = "www.apple.com/iphone-7"
      val sha1 = create(url).head

      val result = Record.getByUrl(url) map {
        case Some(record: Record) => {
          record.sha1 mustEqual sha1
          record.url contains url mustEqual true
          success
        }
        case _ => failure("This url should exist")
      }

      sync {
        result
      }
    }
  }


  "Record.generateSha1" should {

    "should return same hash for same url" in new App {
      val url = "www.apple.com/iphone-7"
      val expectedSha1 = Record.generateSha1(url)
      val actualSha1 = Record.generateSha1(url)
      expectedSha1 mustEqual actualSha1
    }


    "should return different hash for different url with different http prefixes" in new App {
      val url = "http://www.apple.com/iphone-7"
      val cuttedUrl = "https://apple.com/iphone-7"

      val expectedSha1 = Record.generateSha1(url)
      val actualSha1 = Record.generateSha1(cuttedUrl)
      expectedSha1 mustNotEqual actualSha1
    }

    "should return different hash for different urls" in new App {
      val url = "http://www.apple.com/iphone-7"
      val anotherUrl = "http://apple.com"

      val expectedSha1 = Record.generateSha1(url)
      val actualSha1 = Record.generateSha1(anotherUrl)
      expectedSha1 mustNotEqual actualSha1
    }
  }


  "Record.adjustUrl" should {

    "Url should be adjusted to match pattern http://www.domain.region/rest-of-url" in new App {
      val url = "apple.com/iphone-7"
      val expectedUrl = "http://www.apple.com/iphone-7"
      val actualUrl = Record.adjustUrl(url)
      actualUrl mustEqual expectedUrl
    }


    "Adjusting url should be same for url with www and with https://www" in new App {
      val url = "http://www.apple.com/iphone-7"
      val cuttedUrl = "apple.com/iphone-7"

      val actualUrl = Record.adjustUrl(url)
      val expectedUrl = Record.adjustUrl(cuttedUrl)
      actualUrl mustEqual expectedUrl
    }

    "Adjusting url should be same for url without www and with www prefixes" in new App {
      val url = "www.apple.com/iphone-7"
      val cuttedUrl = "apple.com/iphone-7"

      val actualUrl = Record.adjustUrl(url)
      val expectedUrl = Record.adjustUrl(cuttedUrl)
      actualUrl mustEqual expectedUrl
    }

    "If bad address is provided exception should be droped" in new App{
      try{
        Record.adjustUrl("what.iswhat.@")
      }

      catch{
        case uae:UnsupportedAddressTypeException => true mustEqual true
        case _ :Throwable => failure("Another exception was thrown")
      }
    }
  }
}